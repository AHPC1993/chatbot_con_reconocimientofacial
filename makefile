all: install_dependencies


install_dependencies:
	pip install rasa-x --extra-index-url https://pypi.rasa.com/simple
	pip install "rasa[spacy]"
	python -m spacy download es_core_news_sm
	python -m spacy download es_core_news_md
	python -m spacy download en_core_web_md
	python -m spacy link en_core_web_md en
	pip install rasa

init:
	rasa init --no-prompt
	rasa train

run:
	rasa shell